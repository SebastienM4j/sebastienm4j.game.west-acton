package xyz.sebastienm4j.westacton.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import xyz.sebastienm4j.westacton.config.WestActonConfigProperties;
import xyz.sebastienm4j.westacton.game.message.*;

import java.sql.Date;
import java.time.Instant;
import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Service for the game management.
 */
@Service
public class GameService
{
    private static final List<String> FAILURE_OUTPUTS = Arrays.asList(
        "try again...",
            "no, no !...",
            "bad luck",
            "missed !",
            "you have no idea ??",
            "sorry :-/",
            "not yet...",
            "yes it's that ! or not...",
            "you spend the day ?..."
    );

    @Autowired
    private WestActonConfigProperties westActonConfigProperties;

    @Autowired
    private GameSession gameSession;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private TaskScheduler taskScheduler;


    /**
     * Call when the client start.
     *
     * <p>Normally this leads to the start of the game.
     * However, one adapts according to game state.
     */
    public void startup()
    {
        switch(gameSession.getState())
        {
            case WAITING:
                startSession();
                break;
            case STARTING:
                sendStartingMessages();
                break;
            case IN_PROGRESS:
                sendInProgressMessages(false);
                gameSession.setInProgressMessagesSent(true);
                break;
            case GAME_WON:
                sendStartingMessages();
                sendInProgressMessages(false);
                sendSuccessMessages(false, "");
                break;
            case GAME_OVER:
                sendStartingMessages();
                sendInProgressMessages(false);
                sendFailureMessages(false);
                break;
        }
    }

    private void startSession()
    {
        gameSession.start(westActonConfigProperties);

        sendStartingMessages();

        if(westActonConfigProperties.getStartCountdown() == 0) {
            startGame();
        } else {
            taskScheduler.schedule(this::startGame, Date.from(gameSession.getGameBeginning()));
        }
    }

    private void sendStartingMessages()
    {
        simpMessagingTemplate.convertAndSend("/topic/state", gameSession);
        simpMessagingTemplate.convertAndSend("/topic/output", new OutputMessage("downloading the virus archive..."));
    }

    private void startGame()
    {
        gameSession.inProgress();

        taskScheduler.schedule(this::gameOver, Date.from(gameSession.getGameMaximumEnd()));

        sendInProgressMessages(true);
    }

    private void sendInProgressMessages(boolean async)
    {
        simpMessagingTemplate.convertAndSend("/topic/state", gameSession);

        List<WebSocketAsyncMessage> messages = new ArrayList<>();

        messages.add(new AsyncOutputMessage(".", 250));
        messages.add(new AsyncOutputMessage(false, ".", 850));
        messages.add(new AsyncOutputMessage(false, ".", 750));
        messages.add(new AsyncOutputMessage(false, ".", 950));
        messages.add(new AsyncOutputMessage(false, ".", 850));
        messages.add(new AsyncOutputMessage(false, ".", 950));
        messages.add(new AsyncOutputMessage(false, ".", 950));
        messages.add(new AsyncOutputMessage(false, ".", 650));
        messages.add(new AsyncOutputMessage(false, ".", 950));
        messages.add(new AsyncOutputMessage(false, ".", 550));
        messages.add(new AsyncOutputMessage(false, ".", 950));
        messages.add(new AsyncOutputMessage(false, ".", 950));
        messages.add(new AsyncOutputMessage(false, ".", 850));
        messages.add(new AsyncOutputMessage(false, ".", 950));
        messages.add(new AsyncOutputMessage(false, ".", 750));
        messages.add(new AsyncOutputMessage(false, ".", 950));
        messages.add(new AsyncOutputMessage(false, ".", 950));
        messages.add(new AsyncOutputMessage(false, ".", 550));
        messages.add(new AsyncOutputMessage(false, ".", 850));
        messages.add(new AsyncOutputMessage("virus successfully downloaded, extraction...", 350));
        messages.add(new AsyncOutputMessage("inflating crypt84Zio/readme.md", 2680));
        messages.add(new AsyncOutputMessage("inflating crypt84Zio/bin/blocker", 300));
        messages.add(new AsyncOutputMessage("inflating crypt84Zio/bin/blocker.bat", 300));
        messages.add(new AsyncOutputMessage("inflating crypt84Zio/lib/unix/check", 300));
        messages.add(new AsyncOutputMessage("inflating crypt84Zio/lib/unix/crypt", 300));
        messages.add(new AsyncOutputMessage("inflating crypt84Zio/lib/unix/decrypt", 300));
        messages.add(new AsyncOutputMessage("inflating crypt84Zio/lib/win/check.dll", 300));
        messages.add(new AsyncOutputMessage("inflating crypt84Zio/lib/win/crypt.dll", 300));
        messages.add(new AsyncOutputMessage("inflating crypt84Zio/lib/win/decrypt.dll", 300));
        messages.add(new AsyncOutputMessage("starting...", 400));
        messages.add(new AsyncOutputMessage("/!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\", 3450));
        messages.add(new AsyncOutputMessage("&nbsp;&nbsp;He ! I have lock your system !!!", 10));
        messages.add(new AsyncOutputMessage("&nbsp;&nbsp;If you want to unlock it, put the write passphrase", 10));
        messages.add(new AsyncOutputMessage("&nbsp;&nbsp;Good luck !", 10));
        messages.add(new AsyncOutputMessage("/!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\", 10));

        messages.add(new AsyncCommandMessage(true, 200));

        sendMessages(async, messages, () -> gameSession.isInProgressMessagesSent());
    }

    /**
     * Check if the transmitted passphrase is correct.
     *
     * @param passphrase The passphrase to check
     */
    public void checkPassphrase(String passphrase)
    {
        boolean success = westActonConfigProperties.getValidEncryptedAnswers()
                                                   .stream()
                                                   .anyMatch(h -> BCrypt.checkpw(passphrase.toLowerCase(), h));

        if(success)
        {
            gameSession.gameWon();

            sendSuccessMessages(true, passphrase);
        }
        else
        {
            simpMessagingTemplate.convertAndSend("/topic/command", new CommandMessage(passphrase));
            int failureOutputIndex = new Random().nextInt(FAILURE_OUTPUTS.size());
            simpMessagingTemplate.convertAndSend("/topic/output", new OutputMessage(FAILURE_OUTPUTS.get(failureOutputIndex)));
            simpMessagingTemplate.convertAndSend("/topic/command", new CommandMessage(true));
        }
    }

    private void sendSuccessMessages(boolean async, String passphrase)
    {
        simpMessagingTemplate.convertAndSend("/topic/state", gameSession);

        List<WebSocketAsyncMessage> messages = new ArrayList<>();

        messages.add(new AsyncCommandMessage(passphrase, 50));
        messages.add(new AsyncOutputMessage("decrypt file system...", 250));
        messages.add(new AsyncOutputMessage("unlock system...", 1380));
        messages.add(new AsyncOutputMessage("Congratulations !", 1050));
        messages.add(new AsyncCommandMessage(true, 250));

        sendMessages(async, messages);
    }

    private void gameOver()
    {
        gameSession.gameOver();

        sendFailureMessages(true);
    }

    private void sendFailureMessages(boolean async)
    {
        simpMessagingTemplate.convertAndSend("/topic/state", gameSession);

        List<WebSocketAsyncMessage> messages = new ArrayList<>();

        messages.add(new AsyncCommandMessage("./crypt84Zio.sh --over", 50));
        messages.add(new AsyncOutputMessage("removing crypt84Zio...", 250));
        messages.add(new AsyncOutputMessage("Sorry, but you lost !", 1380));
        messages.add(new AsyncOutputMessage("The system will remain locked...", 350));
        messages.add(new AsyncCommandMessage(true, 250));

        sendMessages(async, messages);
    }


    /*
     * Utility methods for send messages
     */

    private void sendMessages(boolean async, List<WebSocketAsyncMessage> messages)
    {
        sendMessages(async, messages, null);
    }

    private void sendMessages(boolean async, List<WebSocketAsyncMessage> messages, Supplier<Boolean> preventAsyncSend)
    {
        Instant nextSent = Instant.now();
        for(WebSocketAsyncMessage m : messages)
        {
            String topic = m instanceof AsyncOutputMessage ? "/topic/output" : "/topic/command";

            if(async)
            {
                nextSent = nextSent.plusMillis(m.getMillisDelay());

                Runnable action;
                if(preventAsyncSend == null) {
                    action = () -> simpMessagingTemplate.convertAndSend(topic, m);
                } else {
                    action = () -> {
                        if(preventAsyncSend.get() == null || !preventAsyncSend.get()) {
                            simpMessagingTemplate.convertAndSend(topic, m);
                        }
                    };
                }

                taskScheduler.schedule(action, Date.from(nextSent));
            }
            else
            {
                simpMessagingTemplate.convertAndSend(topic, m);
            }
        }
    }

}
