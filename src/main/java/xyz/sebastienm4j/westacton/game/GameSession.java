package xyz.sebastienm4j.westacton.game;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import xyz.sebastienm4j.westacton.config.WestActonConfigProperties;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static xyz.sebastienm4j.westacton.game.GameSession.State.*;

/**
 * State of the game session.
 */
@Component
public class GameSession
{
    public enum State
    {
        WAITING,
        STARTING,
        IN_PROGRESS,
        GAME_OVER,
        GAME_WON
    }


    private State state = WAITING;
    private Instant sessionBeginning;
    private Instant gameBeginning;
    private Instant gameMaximumEnd;
    private Instant gameEnd;
    private boolean inProgressMessagesSent;


    /**
     * Start the game session if it's not
     * already started and if it's not finished.
     *
     * @param config Configuration properties (not null)
     */
    public void start(WestActonConfigProperties config)
    {
        Assert.notNull(config, "Game can't be stated, config is required");

        if(!WAITING.equals(state)) {
            return;
        }

        state = STARTING;
        sessionBeginning = Instant.now();
        gameBeginning = sessionBeginning.plusSeconds(config.getStartCountdown());
        gameMaximumEnd = gameBeginning.plus(config.getGameDuration(), ChronoUnit.MINUTES);
    }

    /**
     * Set the game in progress (after countdown).
     */
    public void inProgress()
    {
        if(!STARTING.equals(state)) {
            return;
        }

        state = IN_PROGRESS;
    }

    /**
     * End the game (in progress) with success.
     */
    public void gameWon()
    {
        if(!IN_PROGRESS.equals(state)) {
            return;
        }

        state = GAME_WON;
        gameEnd = Instant.now();
    }

    /**
     * End the game (in progress) without success.
     */
    public void gameOver()
    {
        if(!IN_PROGRESS.equals(state)) {
            return;
        }

        state = GAME_OVER;
    }


    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Instant getSessionBeginning() {
        return sessionBeginning;
    }

    public void setSessionBeginning(Instant sessionBeginning) {
        this.sessionBeginning = sessionBeginning;
    }

    public Instant getGameBeginning() {
        return gameBeginning;
    }

    public void setGameBeginning(Instant gameBeginning) {
        this.gameBeginning = gameBeginning;
    }

    public Instant getGameMaximumEnd() {
        return gameMaximumEnd;
    }

    public void setGameMaximumEnd(Instant gameMaximumEnd) {
        this.gameMaximumEnd = gameMaximumEnd;
    }

    public Instant getGameEnd() {
        return gameEnd;
    }

    public void setGameEnd(Instant gameEnd) {
        this.gameEnd = gameEnd;
    }

    public boolean isInProgressMessagesSent() {
        return inProgressMessagesSent;
    }

    public void setInProgressMessagesSent(boolean inProgressMessagesSent) {
        this.inProgressMessagesSent = inProgressMessagesSent;
    }
}
