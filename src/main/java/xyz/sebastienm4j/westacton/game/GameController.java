package xyz.sebastienm4j.westacton.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import xyz.sebastienm4j.westacton.game.message.PassPhraseMessage;

/**
 * Game controller.
 */
@Controller
public class GameController
{
    @Autowired
    private GameService gameService;


    @GetMapping("/game")
    public String game()
    {
        return "game";
    }


    @MessageMapping("/startup")
    public void startup()
    {
        gameService.startup();
    }

    @MessageMapping("/passphrase")
    public void passphrase(PassPhraseMessage passphrase)
    {
        gameService.checkPassphrase(passphrase.getPassphrase());
    }

}
