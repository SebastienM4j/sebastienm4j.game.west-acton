package xyz.sebastienm4j.westacton.game.message;

/**
 * Web-socket message for set console command prompt.
 */
public class CommandMessage implements WebSocketMessage
{
    private String command;
    private boolean passphraseInput;


    /**
     * New input command message to simulate
     * typed command.
     *
     * @param command Text of the typed command
     */
    public CommandMessage(String command)
    {
        this.command = command;
        this.passphraseInput = false;
    }

    /**
     * New input command message.
     *
     * @param passphraseInput If command prompt is for input passphrase
     */
    public CommandMessage(boolean passphraseInput)
    {
        this.passphraseInput = passphraseInput;
    }


    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public boolean isPassphraseInput() {
        return passphraseInput;
    }

    public void setPassphraseInput(boolean passphraseInput) {
        this.passphraseInput = passphraseInput;
    }
}
