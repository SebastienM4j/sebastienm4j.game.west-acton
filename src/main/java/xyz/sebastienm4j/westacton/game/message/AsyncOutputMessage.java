package xyz.sebastienm4j.westacton.game.message;

/**
 * Web-socket asynchronous message for console output text
 */
public class AsyncOutputMessage extends OutputMessage implements WebSocketAsyncMessage
{
    private int millisDelay;


    /**
     * New output message with text and on new line,
     * potentially sent asynchronously.
     *
     * @param text Text to output
     * @param millisDelay Delay in milliseconds
     */
    public AsyncOutputMessage(String text, int millisDelay)
    {
        super(text);
        this.millisDelay = millisDelay;
    }

    /**
     * New output message with text,
     * potentially sent asynchronously.
     *
     * @param newLine Print on new line or not
     * @param text Text to output
     * @param millisDelay Delay in milliseconds
     */
    public AsyncOutputMessage(boolean newLine, String text, int millisDelay)
    {
        super(newLine, text);
        this.millisDelay = millisDelay;
    }


    @Override
    public int getMillisDelay() {
        return millisDelay;
    }

    public void setMillisDelay(int millisDelay) {
        this.millisDelay = millisDelay;
    }
}
