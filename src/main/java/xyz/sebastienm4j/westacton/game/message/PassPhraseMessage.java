package xyz.sebastienm4j.westacton.game.message;

/**
 * Message sent by client to check the passphrase.
 */
public class PassPhraseMessage
{
    private String passphrase;


    public String getPassphrase() {
        return passphrase;
    }

    public void setPassphrase(String passphrase) {
        this.passphrase = passphrase;
    }

}
