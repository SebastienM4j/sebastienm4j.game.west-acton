package xyz.sebastienm4j.westacton.game.message;

/**
 * Represent a web-socket message payload
 * that could be sent asynchronously.
 */
public interface WebSocketAsyncMessage extends WebSocketMessage
{

    /**
     * @return Delay in milliseconds for asynchronous sent
     */
    int getMillisDelay();

}
