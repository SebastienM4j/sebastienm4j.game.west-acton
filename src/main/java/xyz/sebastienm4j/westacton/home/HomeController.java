package xyz.sebastienm4j.westacton.home;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import xyz.sebastienm4j.westacton.game.GameSession;

import static xyz.sebastienm4j.westacton.game.GameSession.State.WAITING;

/**
 * Home (web entry point) controller.
 */
@Controller
public class HomeController
{
    @Autowired
    private GameSession gameSession;


    @GetMapping("/")
    public String home()
    {
        return WAITING.equals(gameSession.getState())
                ? "home"
                : "game";
    }

}
